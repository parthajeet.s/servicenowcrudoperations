package com.sample.servicenow;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConfigurationProperty;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.api.RemoteFrameworkConnectionInfo;
import org.identityconnectors.framework.api.ResultsHandlerConfiguration;
import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.ConnectorObjectBuilder;
import org.identityconnectors.framework.common.objects.Name;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.ResultsHandler;
import org.identityconnectors.framework.common.objects.SearchResult;
import org.identityconnectors.framework.common.objects.Uid;
import org.identityconnectors.framework.common.objects.filter.EqualsFilter;
import org.identityconnectors.framework.common.objects.filter.Filter;
import org.identityconnectors.framework.common.objects.filter.FilterBuilder;
import org.identityconnectors.framework.common.objects.filter.FilterTranslator;
import org.identityconnectors.framework.impl.api.SearchResultsHandlerLoggingProxy;
import org.identityconnectors.framework.impl.api.local.operations.FilteredResultsHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceNowSampleConnectorApplication {

	public static void main(String[] args) {
		//SpringApplication.run(ServiceNowSampleConnectorApplication.class, args);
		// Use the ConnectorInfoManager to retrieve a ConnectorInfo object for the connector 
		ConnectorInfoManagerFactory fact = ConnectorInfoManagerFactory.getInstance();
		RemoteFrameworkConnectionInfo rinfo = new RemoteFrameworkConnectionInfo("localhost", 8759, new GuardedString("partha".toCharArray()));

		ConnectorInfoManager manager = fact.getRemoteManager(rinfo);
		ConnectorKey key = new ConnectorKey("net.tirasa.connid.bundles.servicenow", "1.0.0", "net.tirasa.connid.bundles.servicenow.SNConnector");       
		ConnectorInfo info = manager.findConnectorInfo(key);

		// From the ConnectorInfo object, create the default APIConfiguration.
		APIConfiguration apiConfig = info.createDefaultAPIConfiguration();

		// From the default APIConfiguration, retrieve the ConfigurationProperties.
		ConfigurationProperties properties = apiConfig.getConfigurationProperties();

		// Print out what the properties are (not necessary)
		List<String> propertyNames = properties.getPropertyNames();
		for(String propName : propertyNames) {
			ConfigurationProperty prop = properties.getProperty(propName);
			System.out.println("Property Name: " + prop.getName() + "\tProperty Type: " + prop.getType());
		}


		// Set all of the ConfigurationProperties needed by the connector.
		properties.setPropertyValue("baseAddress", "https://dev67168.service-now.com");
		properties.setPropertyValue("username", "admin");
		properties.setPropertyValue("password", new GuardedString("D7@k-8fUAeVk".toCharArray()));
		// Use the ConnectorFacadeFactory's newInstance() method to get a new connector.
		ConnectorFacade conn = ConnectorFacadeFactory.getInstance().newInstance(apiConfig);

		// Make sure we have set up the Configuration properly
		conn.validate();
		conn.test();
		
		
		/*CREATE OPERATION*/

		Map<String, Object> map = new HashMap<>();
		map.put("user_name","msd.07");
		map.put("name", "Mahendra Singh Dhoni");
		map.put("email", "msd7@cym.in");
		
		Set<Attribute> set = new HashSet<>();
		for(Entry<String, Object> s : map.entrySet()) {
			set.add(AttributeBuilder.build(s.getKey(), s.getValue()));
		}
		
		System.out.println(set);
		
		Attribute a = null;
		Iterator<Attribute> itr = set.iterator();
		while(itr.hasNext()) {
			a = itr.next();
			System.out.println(a);
		}
		
		conn.create(ObjectClass.ACCOUNT, set, null);
		
		
		/*UPDATE OPERATION*/

		Map<String, Object> map2 = new HashMap<>();
		map2.put("email", "sssss@cym.in");
		Set<Attribute> s1 = new HashSet<>();
		for(Entry<String, Object> s : map2.entrySet()) {
			s1.add(AttributeBuilder.build(s.getKey(),s.getValue()));
		}
	
		conn.update(ObjectClass.ACCOUNT, new Uid("3ce3257d0770111086c5f1e08c1ed0af"), s1,null);
		
		
		/*DELETE OPERATION*/
		//conn.delete(ObjectClass.ACCOUNT, new Uid("3ce3257d0770111086c5f1e08c1ed0af"), null);
		
		
		/*DISPLAY SCHEMA*/

		conn.schema();
		
		
		
		/*SEARCH OPERATION*/

		//ResultsHandler handler = new Tolist
		SearchResult sr = conn.search(ObjectClass.ACCOUNT, FilterBuilder.equalTo(new Name("Partha")), null, null);
		System.out.println(sr.isAllResultsReturned());
	}
}
